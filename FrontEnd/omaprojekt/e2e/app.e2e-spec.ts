import { OmaprojektPage } from './app.po';

describe('omaprojekt App', function() {
  let page: OmaprojektPage;

  beforeEach(() => {
    page = new OmaprojektPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
