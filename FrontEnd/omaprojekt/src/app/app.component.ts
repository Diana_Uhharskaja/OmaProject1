import { Component } from '@angular/core';
import { UserService } from './user.service'

export class User {
  login:string = "";  
  userPassword: string = "";
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  user: User = new User();	
  constructor(private userService: UserService){}
  
  authorize(){	
  	this.userService.authorize(this.user).subscribe((data)=>{
  		console.log(data);
  	});
  }
}
