function getUserId() {
	var url_string = window.location.href;
	var url = new URL(url_string);
	var userId = url.searchParams.get("userId");
	return userId;
}