var allHumansList; // HUMAN VERSION 
function getAllHumansAndFillTable() {
	$.getJSON("http://localhost:8080/OmaProjekt/rest/main/" + getUserId(), function(allHumans) {
		allHumansList = allHumans;
		var tableBodyContent = "";
		for (var i = 0; i < allHumans.length; i++) {
			tableBodyContent = tableBodyContent + "<tr><td>"
				+ allHumans[i].humanId + "</td><td>"
				+ allHumans[i].firstName + "</td><td>"
				+ allHumans[i].lastName + "</td><td>"
				+ formatDate(allHumans[i].humanBirthday)
				+ "</td><td><button type='button' onClick='deleteHuman("
				+ allHumans[i].humanId
				+ ")'>Delete</button></td><td><button type='button' onClick='modifyHuman("
				+ allHumans[i].humanId + ")'>Modify</button></td><td>"
				+ allHumans[i].age + " aastat" + "</td><td>"
				+ "või " + allHumans[i].days + " päeva vana" + "</td></tr>";

		}
		document.getElementById("humansTableBody").innerHTML = tableBodyContent;
	});
}

getAllHumansAndFillTable();

function formatDate(stringDate) {
	var date = new Date(stringDate);
	return date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
}



function deleteHuman(humanId) {
	$.ajax({
		url : "http://localhost:8080/OmaProjekt/rest/main/" + humanId,
		type : "DELETE",
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllHumansAndFillTable();
		},
		error : function(XMLHTTPRequest, textStatus, errorThrown) {
			console.log("error on deleting human");
		}
	});
}

function saveNewHuman() {
	var jsonData = {
		"humanBirthdayStr" : document.getElementById("newHumanBirthday").value,
		"firstName" : document.getElementById("newFirstName").value,
		"lastName" : document.getElementById("newLastName").value,
		"userId" : getUserId()
	};


	var jsonDataString = JSON.stringify(jsonData);

	$.ajax({
		url : "http://localhost:8080/OmaProjekt/rest/main/",
		type : "POST",
		data : jsonDataString,
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllHumansAndFillTable();
		},
		error : function(XMLHTTPRequest, textStatus, errorThrown) {
			console.log("error on adding new human");
		}
	});


}

function modifyHuman(humanId) {
	for (var i = 0; i < allHumansList.length; i++) {
		if (allHumansList[i].humanId == humanId) {
			document.getElementById("modifyHumanId").value = humanId;
			document.getElementById("modifyFirstName").value = allHumansList[i].firstName;
			document.getElementById("modifyLastName").value = allHumansList[i].lastName;
			document.getElementById("modifyHumanBirthday").value = allHumansList[i].humanBirthday.split("T")[0]; // T kohal tükeldamine "2018-02-12T00:00:00+02:00"

		}
	}
}
function saveHumanChanges() {
	var jsonData = {
		"humanId" : document.getElementById("modifyHumanId").value,
		"humanBirthdayStr" : document.getElementById("modifyHumanBirthday").value,
		"firstName" : document.getElementById("modifyFirstName").value,
		"lastName" : document.getElementById("modifyLastName").value
	};

	var jsonDataString = JSON.stringify(jsonData);

	$.ajax({
		url : "http://localhost:8080/OmaProjekt/rest/main",
		type : "PUT",
		data : jsonDataString,
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllHumansAndFillTable();
		},
		error : function(XMLHTTPRequest, textStatus, errorThrown) {
			console.log("error on adding new course type");
		}
	});

}