-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: calendar
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wedding_anniversary_names`
--

DROP TABLE IF EXISTS `wedding_anniversary_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wedding_anniversary_names` (
  `id_wedding_anniversary` int(11) NOT NULL,
  `wedding_anniversary_year` varchar(45) NOT NULL,
  `wedding_anniversary_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id_wedding_anniversary`),
  UNIQUE KEY `wedding_anniversary_year_UNIQUE` (`wedding_anniversary_year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wedding_anniversary_names`
--

LOCK TABLES `wedding_anniversary_names` WRITE;
/*!40000 ALTER TABLE `wedding_anniversary_names` DISABLE KEYS */;
INSERT INTO `wedding_anniversary_names` VALUES (1,'1st YEAR','PAPER '),(2,'2nd YEAR','COTTON '),(3,'3rd YEAR','LEATHER '),(4,'4th YEAR','WOODEN '),(5,'5th YEAR','POTTERY '),(6,'6th YEAR','SUGAR '),(7,'7th YEAR','WOOLEN '),(8,'8th YEAR','COPPER '),(9,'9th YEAR','LINEN '),(10,'10th YEAR','TIN '),(11,'11th YEAR','STEEL '),(12,'12th YEAR','SILK '),(13,'13th YEAR','LACE '),(14,'14th YEAR','IVORY '),(15,'15th YEAR','CRYSTALL '),(16,'16th YEAR','TOPAZ '),(17,'17th YEAR','AMETHYST '),(18,'18th YEAR','TURQUOISE '),(19,'19th YEAR','GRANATE '),(20,'20th YEAR','PORCELAIN '),(21,'21st YEAR','OPAL '),(22,'22nd YEAR','BRONZE '),(23,'23rd YEAR','BERYL '),(24,'24th YEAR','SATIN '),(25,'25th YEAR','SILVER '),(26,'26th YEAR','NEFRITE '),(27,'27th YEAR','MAHOGANY '),(28,'28th YEAR','NICKEL '),(29,'29th YEAR','VELVET '),(30,'30th YEAR','PEARL '),(31,'31st YEAR','BROWN '),(32,'32nd YEAR','COPPER'),(33,'33rd YEAR','STRAWBERRY '),(34,'34th YEAR','AMBER '),(35,'35th YEAR','CORAL '),(36,'36th YEAR','MUSLIN '),(38,'38th YEAR','MERCURY '),(39,'39th YEAR','ALUMINIUM '),(40,'40th YEAR','RUBY '),(41,'41st YEAR','IRON '),(42,'42nd YEAR','NACRE'),(43,'43rd YEAR','FLANELL '),(45,'45th YEAR','SAPHIRE '),(46,'46th YEAR','LAVENDER '),(47,'47th YEAR','CASHMERE '),(49,'49th YEAR','CEDAR '),(50,'50th YEAR','GOLDEN '),(51,'51st YEAR','CAMELLIA '),(52,'52nd YEAR','TOURMALINE '),(53,'53rd YEAR','CHERRY '),(54,'54th YEAR','SABLE '),(55,'55th YEAR','ORCHID '),(56,'56th YEAR','LAZURITE '),(57,'57th YEAR','AZALEA '),(58,'58th YEAR','MAPLE '),(59,'59th YEAR','MINK '),(60,'60th YEAR','DIAMONT '),(65,'65th YEAR','CROWNJEWEL '),(70,'70th YEAR','OAK '),(80,'80th YEAR','PLATINUM '),(90,'90th YEAR','GRANITE '),(100,'100th YEAR','HEAVEN ');
/*!40000 ALTER TABLE `wedding_anniversary_names` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-03 14:35:36
