package ee.bcskoolitus.omaProjekt.resorces;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ee.bcskoolitus.omaProjekt.dao.User;

public class UserResorces {

	public static User saveUser(User newUser) {

		String sqlQuery = "INSERT INTO user (login, user_name, user_surname, user_password) VALUES ('" +
		 newUser.getLogin() + "', '" +
		 newUser.getUserName() + "', '" +
		 newUser.getUserSurname() + "', '" +
		 newUser.getUserPassword() + "');";
		try (Statement statement = DatabaseConnection.getConnection().createStatement();) {
			Integer resultCode = statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			if (resultCode == 1) {
				ResultSet resultSet = statement.getGeneratedKeys();
				while (resultSet.next()) {
					//System.out.println(resultSet.getInt(1));
					newUser.setUserId(resultSet.getInt(1));
				}
			}
		} catch (SQLException e) {
			System.out.println("Error on catching new user in UserResorces.java");
		}
		return newUser;
	}
	public static User authenticateUser(User user) {
		String sqlQuery = "SELECT * FROM user WHERE login='" + user.getLogin() + "' AND user_password='"
				+ user.getUserPassword() + "';";
		try (Statement statement = DatabaseConnection.getConnection().createStatement();) {
			ResultSet results = statement.executeQuery(sqlQuery);
			results.last();
			if (results.getRow() == 1) {
				user.setUserId(results.getInt("user_id"));
				System.out.println(user.getUserId() + " = UserResorces meetodi tulemuse kuvamine authenticateUser");
			}
		} catch (SQLException e) {
			System.out.println("Error on verifying user");
		}
		return user;
	}
}