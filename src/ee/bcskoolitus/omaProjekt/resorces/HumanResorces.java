package ee.bcskoolitus.omaProjekt.resorces;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcskoolitus.omaProjekt.dao.Human;

public class HumanResorces {

	public static List<Human> getAllHumans(int userId) {
		List<Human> allHumans = new ArrayList<>();
		String sqlQuery = "SELECT * FROM human WHERE user_id=" + userId;
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				//System.out.println(results + "human resorces.java human.list results ");
				Human human = new Human(results.getInt("human_id"), results.getString("first_name"),
						results.getString("last_name"), results.getDate("human_birthday"));
				allHumans.add(human);
			}

		} catch (SQLException e) {
			System.out.println("Error on getting all humans" + e.getMessage());
		}

		return allHumans;
	}

	/*public static Human getHumanById(int humanId) {
		Human human = null;
		String sqlQuery = "SELECT * FROM human WHERE human_id=" + humanId;
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				human = new Human(results.getInt("human_id"), results.getString("first_name"),
						results.getString("last_name"), results.getDate("human_birthday"));
			}

		} catch (SQLException e) {
			System.out.println("Error " + e.getMessage());
		}
		return human;
	}*/

	public static Human addHuman(Human newHuman) {

		String sqlQuery = "INSERT INTO human (first_name, last_name, human_birthday, user_id) VALUES ('" +
				newHuman.getFirstName() + "', '" + 
				newHuman.getLastName() + "', '" + 
				newHuman.getHumanBirthdayStr() + "', '" +
				newHuman.getUserId()
				+ "');";
		try (Statement statement = DatabaseConnection.getConnection().createStatement();) {
			Integer resultCode = statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			if (resultCode == 1) {
				ResultSet resultSet = statement.getGeneratedKeys();
				while (resultSet.next()) {
					newHuman.setHumanId(resultSet.getInt(1));
				}
			}
		} catch (SQLException e) {
			System.out.println("Error on catching new human HumanResorces.java");

		}

		return newHuman;

	}

	public static boolean updateHuman(Human humanToUpdate) {
		boolean isUpdateSuccessful = false;
		String sqlQuery = "UPDATE human SET first_name='" + humanToUpdate.getFirstName() + "', last_name='"
				+ humanToUpdate.getLastName() + "', human_birthday='" + humanToUpdate.getHumanBirthdayStr()
				+ "' WHERE human_id=" + humanToUpdate.getHumanId();
		System.out.println(sqlQuery);
		try (Statement statement = DatabaseConnection.getConnection().createStatement();) {
			Integer resultCode = statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			if (resultCode == 1) {
				isUpdateSuccessful = true;
			}
		} catch (SQLException e) {
			System.out.println("Error on human update in HumanResorces.java");
		}
		return isUpdateSuccessful;
	}

	public static boolean deleteHumanById(int humanId) {
		boolean isDeleteSuccesful = false;
		String sqlQuery = "DELETE FROM human " + "WHERE human_id=" + humanId;
		try (Statement statement = DatabaseConnection.getConnection().createStatement();) {
			Integer resultCode = statement.executeUpdate(sqlQuery);
			if (resultCode == 1) {
				isDeleteSuccesful = true;
			}
		} catch (SQLException e) {
			System.out.println("Error deleting human in HumanResorces.java");
		}
		return isDeleteSuccesful;
	}
}
