package ee.bcskoolitus.omaProjekt.dao;

public class User {
	
	private int userId;
	private String login;
	private String userPassword;
	private String userName;
	private String userSurname;
	
	public User () {
		
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserSurname() {
		return userSurname;
	}

	public void setUserSurname(String userSurname) {
		this.userSurname = userSurname;
	}

	@Override
	public String toString() {
		return "User[userId=" + userId + ", login=" + login + ", userPassword=" + userPassword + ", userName="
				+ userName + ", userSurname=" + userSurname + "]";
	}

}

