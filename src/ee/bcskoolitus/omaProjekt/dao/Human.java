package ee.bcskoolitus.omaProjekt.dao;

import java.text.ParseException;
import java.util.concurrent.TimeUnit;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Human {

	private int humanId;
	private String firstName;
	private String lastName;
	private Date humanBirthday;
	private String humanBirthdayStr;
	private int age;
	private int days;
	private int userId;

	public Human() {
	}

	public Human(int humanId, String firstName, String lastName, Date humanBirthday) {
		this.humanId = humanId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.humanBirthday = humanBirthday;
		this.age = calculateAge();
		this.days = calculateDays();
	}

	public int getHumanId() {
		return humanId;
	}

	public void setHumanId(int humanId) {
		this.humanId = humanId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getHumanBirthday() { // format on "2018-02-12T00:00:00+02:00"
		return humanBirthday;
	}

	public void setHumanBirthday(Date humanBirthday) {
		this.humanBirthday = humanBirthday;
	}

	public void setHumanBirthday(String humanBirthday) {
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		try {
			this.humanBirthday = simpleDateFormat.parse(humanBirthday);
			System.out.println(this.humanBirthday + "humanBirthday");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public String getHumanBirthdayStr() { // funktsioon "add Human" jaoks. input --> mySQL format "2018-02-12"
		return humanBirthdayStr;
	}

	public void setHumanBirthdayStr(String humanBirthdayStr) {
		System.out.println(humanBirthdayStr + "humanBirthdayStr");

		this.humanBirthdayStr = humanBirthdayStr;
		setHumanBirthday(humanBirthdayStr);
	}

	
	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	private int calculateAge() {
		long curDate = new Date().getTime();
		long humanBrdTime = this.humanBirthday.getTime();

		int fullYear = (int) TimeUnit.MILLISECONDS.toDays(curDate - humanBrdTime) / 365;
		// System.out.println(this.humanBirthday.getTime());

		return fullYear;
	}
	
	private int calculateDays() {
		long curDate = new Date().getTime();
		long humanBrdTime = this.humanBirthday.getTime();
		
		int fullDays = (int) TimeUnit.MILLISECONDS.toDays(curDate - humanBrdTime);
		return fullDays;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "Human[humanId=" + humanId + ", firstName=" + firstName + ", lastName=" + lastName + ", birthday="
				+ humanBirthday + ", age=" + age + ", days=" + days + ", userId=" + userId + "]";
	}
}
