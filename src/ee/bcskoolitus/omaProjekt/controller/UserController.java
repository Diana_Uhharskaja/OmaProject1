package ee.bcskoolitus.omaProjekt.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.omaProjekt.dao.User;
import ee.bcskoolitus.omaProjekt.resorces.UserResorces;

@Path("/index")
public class UserController {
	
	@Path("/addinguser")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User saveNewUser(User newUser) {
		return UserResorces.saveUser(newUser);
	}
	
	@Path("/authentication")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User authenticateUser(User user) {
		return UserResorces.authenticateUser(user);
				 
	}

}

