package ee.bcskoolitus.omaProjekt.controller;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcskoolitus.omaProjekt.dao.Human;
import ee.bcskoolitus.omaProjekt.resorces.HumanResorces;

@Path("/main")
public class HumanController {
			
			@GET
			@Path("/{userId}")
			@Produces(MediaType.APPLICATION_JSON)
			public List<Human> getAllHumans(@PathParam("userId") int userId) {
				return HumanResorces.getAllHumans(userId);
			}
			
			
			@POST
			@Consumes(MediaType.APPLICATION_JSON)
			@Produces(MediaType.APPLICATION_JSON)
			public Human addNewHuman(Human newHuman) {	
				System.out.println(newHuman + " human controller addNewHuman");
				return HumanResorces.addHuman(newHuman);
			}
			
			
			
			@DELETE
			@Path("/{humanId}")
			public void deleteHumanById(@PathParam("humanId") int humanId) {
				HumanResorces.deleteHumanById(humanId);
			}
			
			@PUT
			@Consumes(MediaType.APPLICATION_JSON)
			@Produces(MediaType.TEXT_PLAIN)
			public boolean updateHuman(Human human) {
				return HumanResorces.updateHuman(human);
			}
}
/*Аннотация @Path указывает путь, по которому будет доступен наш сервис. 
 * Аннотация @GET/POST/PUT определяет, какой HTTP-запрос будет обрабатываться данным методом. 
 * Аннотация @Produces позволяет указать, в каком формате данный сервис предоставляет результаты.
 */

